<?

// Файл получает последние 10 новостей из ИБ с id 3 и выводит их в формате JSON

require_once('../bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

// Добавляю сортировку по убыванию, по дате создания
$arOrder = ['ACTIVE_FROM' => 'DESC'];
// Фильтрую вывод по ИБ с id = 3
$arFilter = ['IBLOCK_ID' => 3];
// Вывожу только те поля, которые надо (PROPERTY_70 - RESPONSIBLE - ответственный; PROPERTY_71 - ADDRESS - адрес;  PROPERTY_72 - COORDINATES - координаты) 
$arSelect = ['NAME', 'ACTIVE_FROM', 'PREVIEW_TEXT', 'PROPERTY_70', 'PROPERTY_71', 'PROPERTY_72'];
// Ограничиваю вывод последними 10 записями
$limit = ["nPageSize" => 10];

$res = CIBlockElement::GetList($arOrder, $arFilter, false, $limit, $arSelect);

while($ob = $res->GetNextElement(true, false))  {  
    $arFields = $ob->GetFields(); 
    // Заменяю названия ключей, чтобы было понятнее, какое поле за что отвечает
    $arFields['RESPONSIBLE'] = $arFields['PROPERTY_70_VALUE'];
    $arFields['ADDRESS'] = $arFields['PROPERTY_71_VALUE'];
    $arFields['COORDINATES'] = $arFields['PROPERTY_72_VALUE'];
    // Удаляю ненужные элементы массива
    unset($arFields['PROPERTY_70_VALUE'], $arFields['PROPERTY_70_VALUE_ID'], $arFields['PROPERTY_71_VALUE'], $arFields['PROPERTY_71_VALUE_ID'], $arFields['PROPERTY_72_VALUE'], $arFields['PROPERTY_72_VALUE_ID'], $arFields['PREVIEW_TEXT_TYPE']);
    $arData[] = $arFields;
} 
// Кодирую в формат JSON
$jsonData = \Bitrix\Main\Web\Json::encode($arData, JSON_UNESCAPED_UNICODE);
echo $jsonData;

?>