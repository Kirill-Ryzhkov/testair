<?

// Файл получает строку JSON, обрабатывает ее и добавляет новости в ИБ с id = 14, Новости 2

require_once('../bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule('iblock');

$elem = new CIBlockElement;

// Получаю JSON строку с данными и декодирую строку JSON в массив, для удобства использования
$getJson = \Bitrix\Main\Web\Json::decode(file_get_contents('http://localhost:8888/news2/getNews.php'));

foreach($getJson as $json){
    $name = $json['NAME'];
    $date = $json['ACTIVE_FROM'];
    $preview = $json['PREVIEW_TEXT'];
    $responsible = $json['RESPONSIBLE'];
    $address = $json['ADDRESS'];
    $coordinates = $json['COORDINATES'];
    // Формирую новый массив с данными для созднания новости в ИБ Новости 2
    $arData = [
        'MODIFIED_BY' => 1, 
        'IBLOCK_ID' => 14,
        'ACTIVE' => 'Y',
        'NAME' => $name, 
        'ACTIVE_FROM' => $date, 
        'PREVIEW_TEXT' => $preview,
        'PROPERTY_VALUES' => [
            73 => $responsible, 
            74 => $address, 
            75 => $coordinates, 
        ]
    ];
    // Проверяю существует ли такая новость, которую я хочу добавить, чтобы избежать дубликатов новостей
    $res = CIBlockElement::GetList(
        array("SORT" => "ASC"), 
        array(
            "IBLOCK_ID" => $arData['IBLOCK_ID'],
            "NAME" => $arData['NAME'],
            "PREVIEW_TEXT" => $arData['PREVIEW_TEXT']
        )
    );
    
    // Если приходит не пустой объект, то значит, такая новость есть
    if($ob = $res->GetNextElement()){} 
    // иначе новости нет и можно добавить новую
    else {
        $elem->Add($arData);
    }
}



